# 响应式布局 + flex布局 的 vue 项目demo

## 需要用到 vue + scss

 :facepunch: 共4步,写死的那种,也许还可以精炼,按那四句代码加也可以,在app.vue 和 global.scss实现文件里面,很简单没多少,有基础20分钟就能全部看懂. :+1: ,app.vue的scss(写的flex)和引入的global.scss(写的@media)里写了 两个重点 @media 和 flex 布局 .


 :star: 若不了解以上媒体查询 或是 flex布局 可具体查看以下两篇教程

1. [自适应网页设计（Responsive Web Design）--阮一峰](http://www.ruanyifeng.com/blog/2012/05/responsive_web_design.html) 

2. [flex布局-阮一峰](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)  

 **以上俩个教程强烈推荐.**  :+1: 